@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('Digitpapeles online -Página Principal') }} <!-- Titulo de la página  ESTA NO ES LA VISTA-->
@endsection

@section('namelg1_title')  <!-- Titulo menú lado izquierdo extendido -->
	{{ trans('PAGINA DE PRUEBA') }}
@endsection

@section('namelg2_title')
	{{ trans('Papers') }}
@endsection

@section('namemini1_title') <!-- Titulo menú lado izquierdo reducido -->
	{{ trans('D') }}
@endsection

@section('namemini2_title')
	{{ trans('P') }}
@endsection

@section('contentheader_title') <!-- Titulo Panel Derecho esquina superior izquierda -->
  {{ trans('Dashboard') }}
@endsection

@section('contentheader_description')
  {{ trans('Control Panel') }}
  
@endsection


@section('main-content')
	<!-- Main content -->
	 
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$archivos->count()}}</h3>

              <p>Archivos Procesados Diarios</p>
            </div>
            <div class="icon">
              <i class="ion ion-upload"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>1<sup style="font-size: 20px">%</sup></h3>

              <p>Tasa de Fallo</p>
            </div>
            <div class="icon">
              <i class="ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>0.5seg</h3>

              <p>Tiempo de Procesamiento</p>
            </div>
            <div class="icon">
              <i class="ion ion-speedometer"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>950</h3>

              <p>Archivos Totales</p>
            </div>
            <div class="icon">
              <i class="ion ion-folder"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
	  
	   <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Reporte de Archivos Cargados por Mes vs Archivos con error</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <p class="text-center">
                    <strong>Archivos Cargados: 1 Ago, 2018 - 07 Nov, 2018</strong>
                  </p>

                  <div class="chart">
                    <!-- Sales Chart Canvas -->
                    <canvas id="salesChart" style="height: 180px;"></canvas>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <p class="text-center">
                    <strong>Cargas por Compañía</strong>
                  </p>

                  <div class="progress-group">
                    <span class="progress-text">HDI</span>
                    <span class="progress-number"><b>318</b>/950</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="width: 34%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Liberty</span>
                    <span class="progress-number"><b>258</b>/950</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: 27%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">BCI</span>
                    <span class="progress-number"><b>228</b>/950</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: 24%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Chilena</span>
                    <span class="progress-number"><b>146</b>/950</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 15%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
				    <h5 class="description-header text-gray">VProceso 1.5 seg<i class="fa fa-caret-up"></i></h5>
                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> Fallo 0.6%</span>                    
                    <br><span class="description-text">AGOSTO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
				    <h5 class="description-header text-gray">VProceso 1.1 seg<i class="fa fa-caret-up"></i></h5>
                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> Fallo 0.5%</span>                    
                    <br><span class="description-text">SEPTIEMBRE</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
				    <h5 class="description-header text-gray">VProceso 0.9 seg <i class="fa fa-caret-up"></i></h5>
                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> Fallo 1%</span>                    
                    <br><span class="description-text">OCTUBRE</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
				    <h5 class="description-header text-gray">VProceso 0.7 seg<i class="fa fa-caret-up"></i></h5>
                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> Fallo 0.3%</span>                    
                    <br><span class="description-text">NOVIEMBRE</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->   




	   
	  <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Porcentaje de Fallos </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <div class="chart-responsive">
                    <canvas id="pieChart" height="150"></canvas>
                  </div>
                  <!-- ./chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <ul class="chart-legend clearfix">
                    <li><i class="fa fa-circle-o text-red"></i> Archivos cargados con error</li>                    
                    <li><i class="fa fa-circle-o text-aqua"></i> Archivos cargados sin error</li>                  
                  </ul>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#">%Archivos con error
                  <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 2%</span></a></li>
                <li><a href="#">%Archivos sin error <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 98%</span></a>
                </li>                
              </ul>
            </div>
            <!-- /.footer -->
          </div>
          <!-- /.box -->
	  
	  
	  
	  </section>
    <!-- /.content -->
  
@endsection

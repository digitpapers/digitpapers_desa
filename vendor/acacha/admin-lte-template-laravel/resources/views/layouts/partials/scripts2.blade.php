<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<!-- ChartJS -->

<!-- Scripts -->


<!-- End Scripts -->




<script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script> 
<script src="/js/jquery/dist/jquery.min.js"></script>
<script src="/js/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/js/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/js/adminlte.min.js"></script>
<script src="/js/chart.js/Chart.js"></script>
<script src="/js/dashboard2.js" type="text/javascript"></script>

<script>
function Buscar_Propuestas()
{
	var compania 	= document.getElementById('compania').value;
	var npropuesta 	= document.getElementById('npropuesta').value;
	var nrut 		= document.getElementById('nrut').value;
	var estado 		= document.getElementById('estado').value;
	var fini 		= document.getElementById('fini').value;
	var ffin 		= document.getElementById('ffin').value;
	
	if (compania=="")
	{
		compania="NA"
	}
	if (npropuesta=="")
	{
		npropuesta="NA"
	}
	if (nrut=="")
	{
		nrut="NA"
	}
	else
	{
		if ((nrut.length>8) || isNaN(nrut))
		{
			alert("Introduzca un Número de Rut Válido. Recuerde ingresarlo sin el dv y sin el guión");
			document.getElementById('nrut').focus();
			return
			
		}
	}
	
	if (estado=="")
	{
		estado="NA"
	}
	
	if (fini=="")
	{
		fini="NA"
	}
	
	if (ffin=="")
	{
		ffin="NA"
	}
	
	if((compania=="NA") && (npropuesta=="NA") && (nrut=="NA") && (estado=="NA") && (fini=="NA") && (ffin=="NA"))
	{
		document.location.href="/products";
	}
	else
	{
		document.location.href="/buscar_propuestas/"+compania+"/"+npropuesta+"/"+nrut+"/"+estado+"/"+fini+"/"+ffin;
	}
	
	
}

function Confirmar( ) 
{   
 	var compania 	= document.getElementById('compania').value;
    var npropuesta	= document.getElementById('npropuesta').value;
	    npropuesta	= npropuesta.trim();
	 
	var tipoarchivo	= document.getElementById('tipoarchivo').value;
	var id_archivo	= document.getElementById('id_archivo').value;
	var patron		= /^\d*$/;
	var okcomp		=1;
	var oktipo		=1;
	var okprop		=1;
	
	
	if (compania=="")  
	{
		alert("Introduzca una compañía");
		document.getElementById('compania').focus();
		okcomp=0;
		return false;
	}
	
	if (tipoarchivo=="")  
	{
		alert("Introduzca un tipo de archivo");
		document.getElementById('tipoarchivo').focus();
		oktipo=0;
		return false;
	}
	
	if ((isNaN(npropuesta)) || (npropuesta.length==0) || (npropuesta.length>10))
	{
		alert("Introduzca un nombre de propuesta válido");		
		document.getElementById('npropuesta').focus();
		okprop=0;
		return false;
	}
	
    if ((okcomp==1)&&(oktipo==1)&&(okprop==1))
	{
		if(confirm("¿Seguro desea actualizar este archivo?. Los cambios no podrán ser reversados posteriormente"))
		{
			
			document.location.href="/actualizar_archivo/"+compania+"/"+npropuesta+"/"+tipoarchivo+"/"+id_archivo;
		}
		else
		{
			//document.location.href="no.html"; 
		}
	}

	
};

function Confirmar_duplicados(npropuesta ) 
{   
    document.location.href="/confirmar_duplicado/"+npropuesta;	
};

function eliminar_duplicados( ) 
{   
	
   
    
	if(confirm("¿Seguro desea dejar el archivo seleccionado como principal y eliminar los que no están marcados?. Los cambios no podrán ser reversados posteriormente"))
	{
		var eliminados = document.getElementsByName("eliminado");
		var i;
		var cont=0;
		var idarchivo_aeliminar;
		
		for(i=0;i< eliminados.length;i++)
		{  
	        
			if(eliminados[i].checked)
			{
				cont++;
				idarchivo_aeliminar=eliminados[i].value;
				
			}
			
		}
		;
		if(cont==1)
		{
			document.location.href="/eliminar_archivo/"+idarchivo_aeliminar;
		}
		else
		{
			alert("Debe marcar un solo archivo, el cual será dejado como principal");
		}
	}
	else
	{
		//document.location.href="no.html"; 
	}

	
};


  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : false,
      'autoWidth'   : false
    })
	$('#example3').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
	  'language'    : {
						'lengthMenu': 'Mostrar _MENU_ Entradas',
						'info': 'Mostrando _START_ a _END_ de _TOTAL_ Entradas',
						'infoEmpty': 'Mostrando 0 a 0 de 0 Entradas',
						'infoFiltered': '(Filtrado de _MAX_ total entradas)',
				        'paginate': {
				                      'previous': 'Anterior',
									  'next'    : 'Siguiente'
				                    }
				      }
	  
    })
  })
  
  var filterByDate = function() {
						$.fn.dataTableExt.afnFiltering.push(
						   function( oSettings, aData, iDataIndex ) {
							  var fechaCarga = aData[5]; //Aqui obtienes el valor de la columna.
							  var filtro4= $('#fini').val();
							  var filtro5= $('#ffin').val();
							 							
							  if(fechaCarga >= filtro4 && fechaCarga<=filtro5)
							  { 
								 return true;
							  }
							  else if (filtro4=='' && filtro5=='')
							  {  
								 return true;
							  }
							  else
							  {
								  return false;
							  }
							  
							  
						   }
						);
					};
  
  window.onload = function(){
	  
	  function setear_fechas()
	  {
		  var fecha = new Date(); //Fecha actual	  
		  var mes = fecha.getMonth()+1; //obteniendo mes
		  var dia = fecha.getDate(); //obteniendo dia
		  var ano = fecha.getFullYear(); //obteniendo año
		  if(dia<10)
		  {
			dia='0'+dia; //agrega cero si el menor de 10
		  }
		  if(mes<10)
		  {
			mes='0'+mes //agrega cero si el menor de 10
		  }
			
		  
		  document.getElementById('ffin').value=ano+"-"+mes+"-"+dia;
		  
		  var finicio	= new Date();
			  finicio.setDate(finicio.getDate()-10); 
		  var mesinicio = finicio.getMonth()+1; //obteniendo mes
		  var diainicio = finicio.getDate(); //obteniendo dia
		  var anoinicio = finicio.getFullYear(); //obteniendo año
		  if(diainicio<10)
		  {
			diainicio='0'+diainicio; //agrega cero si el menor de 10
		  }
		  if(mesinicio<10)
		  {
			mesinicio='0'+mesinicio //agrega cero si el menor de 10
		  }
		  
		  document.getElementById('fini').value=anoinicio+"-"+mesinicio+"-"+diainicio;
        }	 
			
		setear_fechas();	
	};
  
  
  
  
   $(document).ready(function() {
		      
			   
			   /* $("#submit").click(function(){
					var oTable = $('#example1').dataTable();
					var filtro1= $('select[id=compania]').val();
					var filtro2= $('input:text[name=npropuesta]').val()
					var filtro3= $('select[id=estado]').val();
					var filtro4= $('#fini').val();
					var filtro5= $('#ffin').val();
					var filtro6= $('input:text[name=nrut]').val()
					
							
		 
				   
					 if (filtro1!='Seleccionar Compañía' )
					 {	 
						oTable.fnFilter(filtro1,1,false,false,true);
					 }
					 else
					 {
						 oTable.fnFilter('',1,false,false,true);
					 }
					 
					
					 if (filtro2!='' ) 
					 {	 
						oTable.fnFilter(filtro2,2,false,false,true);	
					 }
					 else
					 {
						 oTable.fnFilter('',2,false,false,true);
					 }
					
					if (filtro3!='Seleccionar Estado')
					{
						oTable.fnFilter(filtro3,7,false,false,true);
					}
					else
					{
						oTable.fnFilter('',7,false,false,true);
					}	
					
					if (filtro6!='' ) 
					 {	 
						oTable.fnFilter(filtro6,3,false,false,true);	
					 }
					 else
					 {
						 oTable.fnFilter('',3,false,false,true);
					 }
					
					filterByDate();
					oTable.fnDraw();				
					
					
					
				});*/
			   
			   
			   
		     } );
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->

<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="#"></a><b>Digitpapers</b></a>. {{ trans('Un producto para reducir costos y optimizar tus recursos') }}
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="#">Digitpapers.com</a>.</strong> {{ trans('adminlte_lang::message.createdby') }} <a href="#">Digitpapers</a>
</footer>

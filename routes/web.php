<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
/*Route::get('/', function () 
{
    //return view('welcome');
	
});*/



Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
    //        // Uses Auth Middleware
    //    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});

//Route::get('products', 'ProductsController@index');
//Route::get('products', 'archivosController@show');
//Route::resource('archivos', 'archivosController');
Route::get('/home', 'archivosController@showdashboard');


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/buscar_propuestas/{compania}/{npropuesta}/{rut}/{estado}/{fini}/{ffin}', 'archivosController@buscar_propuestas');
Route::get('/products', 'archivosController@show');
Route::get('/download/{file}/{nombre}', 'archivosController@download'); 
Route::get('/vercontenido/{id}', 'archivosController@vercontenido'); 
Route::get('/eliminar/{Id}/{Nombre}/{FechaCarga}/{Tamano}/{Estado}/{Tipo}/{Propuesta}', 'archivosController@eliminar'); 
Route::get('/eliminar_archivo/{id}', 'archivosController@eliminar_archivo'); 
Route::get('/actualizar/{Id}/{FechaCarga}/{CodError}', 'archivosController@actualizar'); 
Route::get('/actualizar_archivo/{compania}/{npropuesta}/{tipoarchivo}/{id_archivo}', 'archivosController@actualizar_archivo'); 
Route::get('/confirmar_duplicado/{npropuesta}', 'archivosController@confirmar_duplicado'); 
 

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Connection;


class archivos extends Model
{
    //
	//protected $fillable = ['id_cliente', 'id_archivo', 'fecha_carga','fecha_ini','fecha_fin','nombre_ini','nombre_fin','tamano','path','cod_error','msg_error','cod_cia','cod_propuesta'];
	protected $fillable = ['Id', 'ProyectoId', 'CompaniaId','CodCompania','NroPropuesta','RutCliente','Ruta','NroArchivosTotal','NroArchivosError','NroArchivosDuplicado','FechaCarga','CodError','MsgError'];
}
/*
class SetArchivo extends Model
{
    //
	protected $fillable = ['Id', 'ProyectoId', 'CompaniaId','CodCompania','NroPropuesta','RutCliente','Ruta','NroArchivosTotal','NroArchivosError','NroArchivosDuplicado','FechaCarga','CodError','MsgError'];
}*/
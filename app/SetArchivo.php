<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Connection;



class SetArchivo extends Model
{
    //
	protected $fillable = ['Id', 'ProyectoId', 'CompaniaId','CodCompania','NroPropuesta','RutCliente','Ruta','NroArchivosTotal','NroArchivosError','NroArchivosDuplicado','FechaCarga','CodError','MsgError'];
}
<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class ProductsController extends Controller
{
  public function index()
   {
     $data = [
        "title" => "Productos",
        "products" => [
            ["name" => "Producto 1", "price" => 2.4],
            ["name" => "Producto 2", "price" => 1.4],
            ["name" => "Producto 3", "price" => 0.4],
        ]
    ];
    return view('products')->with($data);
   }
   
}
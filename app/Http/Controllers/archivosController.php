<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\archivos;
use App\SetArchivo;
use DB;


class archivosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		echo("Entre index");
		$archivos=archivos::orderBy('id','DESC')->paginate(3);
        return view('products.blade',compact('archivos')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		echo("Entre create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		echo("Entre store");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
       
		//$archivos=archivos::all();
		$archivos = DB::table('SetArchivo')->get();		
		return  view('products',compact('archivos'));
    }

	public function showdashboard()
    {
        //
		//$archivos=archivos::all();
		
				
		$archivos      					 = archivos::raw('EXEC ARCHIVOSBUSCA ');
		$archi         				 	 = DB::select('exec DIGITPAPER_DASHBOARD',array());
		$tasafallo     					 = $archi[0]->tasa_fallos;
		$totalarchivos 					 = $archi[0]->total_archivos;
		$total_errores 					 = $archi[0]->total_errores;
		$total_duplicados				 = $archi[0]->total_duplicados;
		$total_archivos_sinerror		 = $archi[0]->total_archivos_sinerror;
		$tiempo_proceso					 = $archi[0]->tiempo_proceso;
		$archivos_cargados_mes			 = $archi[0]->archivos_cargados_mes;
		$archivos_cargados_dia			 = $archi[0]->archivos_cargados_dia;
		$tasa_aciertos					 = $archi[0]->tasa_aciertos;
		$tasa_duplicados				 = $archi[0]->tasa_duplicados;
		$cont_chilena					 = $archi[0]->cont_chilena;
		$cont_consorcio				 	 = $archi[0]->cont_consorcio;
		$cont_bci						 = $archi[0]->cont_bci;
		$cont_hdi						 = $archi[0]->cont_hdi;
		$cont_liberty					 = $archi[0]->cont_liberty;
		$cont_sura						 = $archi[0]->cont_sura;
		$primer_mes_grafico				 = $archi[0]->primer_mes_grafico;
		$segundo_mes_grafico			 = $archi[0]->segundo_mes_grafico;
		$tercer_mes_grafico				 = $archi[0]->tercer_mes_grafico;
		$anio_actual		             = $archi[0]->anio_actual;
		$anio				             = $archi[0]->anio;
		$archivos_cargados_mes1 		 = $archi[0]->archivos_cargados_mes1;
		$archivos_cargados_mes2 		 = $archi[0]->archivos_cargados_mes2;
		$archivos_cargados_mes1_conerror = $archi[0]->archivos_cargados_mes1_conerror;
		$archivos_cargados_mes2_conerror = $archi[0]->archivos_cargados_mes2_conerror;
		$archivos_cargados_mes_conerror  = $archi[0]->archivos_cargados_mes_conerror;
		
		$velocidad_carga_mes1			 = $archi[0]->velocidad_carga_mes1;
		$velocidad_carga_mes2			 = $archi[0]->velocidad_carga_mes2;
		$velocidad_carga_mes			 = $archi[0]->velocidad_carga_mes;
		
		
		if ($totalarchivos>0)
		{	
			$porcentaje_fallo_mes1			 = ($archivos_cargados_mes1<>0) ?(($archivos_cargados_mes1_conerror/$archivos_cargados_mes1)*100):0;
			$porcentaje_fallo_mes2			 = ($archivos_cargados_mes2<>0) ?(($archivos_cargados_mes2_conerror/$archivos_cargados_mes2)*100):0;
			$porcentaje_fallo_mes			 = ($archivos_cargados_mes <>0) ?(($archivos_cargados_mes_conerror/$archivos_cargados_mes)*100):0;
			
			$porc_chilena					 = ($cont_chilena/$totalarchivos)*100;
			$porc_consorcio					 = ($cont_consorcio/$totalarchivos)*100;
			$porc_bci						 = ($cont_bci/$totalarchivos)*100;
			$porc_hdi						 = ($cont_hdi/$totalarchivos)*100;
			$porc_liberty					 = ($cont_liberty/$totalarchivos)*100;
			$porc_sura						 = ($cont_sura/$totalarchivos)*100;
			
		}
		else
		{   
			$porcentaje_fallo_mes1			 = 0;
			$porcentaje_fallo_mes2			 = 0;
			$porcentaje_fallo_mes			 = 0;
			
			$porc_chilena					 = 0;
			$porc_consorcio					 = 0;
			$porc_bci						 = 0;
			$porc_hdi						 = 0;
			$porc_liberty					 = 0;
			$porc_sura						 = 0;
		}
		 
		
        //return  view('home',compact('archivos'));
		return  view('home',[
								'tasafallo'        					=> $tasafallo,
								'totalarchivos'    					=> $totalarchivos,
								'archivos'         			        => $archivos,
								'tiempo_proceso'   					=> $tiempo_proceso,
								'archivos_cargados_mes'				=> $archivos_cargados_mes,
								'archivos_cargados_dia'				=> $archivos_cargados_dia,
								'total_errores'						=> $total_errores,
								'total_duplicados'					=> $total_duplicados,
								'total_archivos_sinerror'			=> $total_archivos_sinerror,
								'tasa_aciertos'						=> $tasa_aciertos,	
								'tasa_duplicados'					=> $tasa_duplicados,
								'cont_chilena'						=> $cont_chilena,
								'cont_consorcio'					=> $cont_consorcio,
								'cont_bci'							=> $cont_bci,
								'cont_hdi'							=> $cont_hdi,
								'cont_liberty'						=> $cont_liberty,
								'cont_sura'							=> $cont_sura,							
								'porc_chilena'						=> $porc_chilena,
								'porc_consorcio'					=> $porc_consorcio,
								'porc_bci'							=> $porc_bci,
								'porc_hdi'							=> $porc_hdi,
								'porc_liberty'						=> $porc_liberty,
								'porc_sura'							=> $porc_sura,
								'primer_mes_grafico'				=> $primer_mes_grafico,
								'segundo_mes_grafico'				=> $segundo_mes_grafico,
								'tercer_mes_grafico'				=> $tercer_mes_grafico,
								'anio_actual'						=> $anio_actual,
								'anio'								=> $anio,
								'archivos_cargados_mes1'			=> $archivos_cargados_mes1,
								'archivos_cargados_mes2'			=> $archivos_cargados_mes2,
								'archivos_cargados_mes1_conerror'	=> $archivos_cargados_mes1_conerror,
								'archivos_cargados_mes2_conerror'	=> $archivos_cargados_mes2_conerror,
								'archivos_cargados_mes_conerror'	=> $archivos_cargados_mes_conerror,
								'porcentaje_fallo_mes1'				=> round($porcentaje_fallo_mes1,2),
								'porcentaje_fallo_mes2'				=> round($porcentaje_fallo_mes2,2),
								'porcentaje_fallo_mes'				=> round($porcentaje_fallo_mes,2),
								'velocidad_carga_mes1'				=> $velocidad_carga_mes1,
								'velocidad_carga_mes2'				=> $velocidad_carga_mes2,
								'velocidad_carga_mes'				=> $velocidad_carga_mes
								
                            ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		echo("Entre edit");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		
		
    }
	
	 public function download($id_archivo,$NombreInicio)
    {
        
		$archi         = DB::select('exec DIGITPAPER_BUSCAR_PATH ?', array($id_archivo));			
		$path          = $archi[0]->ruta;
		//echo($path);
		//$path          = 'C:\Users\Administrador.WIN-8M5G07AD6HN\OneDrive\DigitPapers\Derco\Storage\Propuestas_Digitalizadas\03\0000021572\0000021572.pdf';
		
		
		return response()->file($path);		
		
    }
	
	 public function buscar_propuestas($compania,$npropuesta,$rut,$estado,$fini,$ffin)
    {
        
		$archivos         = DB::select('exec DIGITPAPER_BUSCAR_PROPUESTAS ?,?,?,?,?,?', array($compania,$npropuesta,$rut,$estado,$fini,$ffin));
        //echo (empty($archivos));	
				
		//return  view('products', ['archivos'    => $archivos]);
		return  view('products',compact('archivos'));
		
    }
	
	
	
	
	
	
	 public function vercontenido($id_setarchivo)
    {
        
		$archi         = DB::select('exec DIGITPAPER_BUSCAR_ARCHIVOS_ASOCIADOS_PROPUESTA ?', array($id_setarchivo));
        $archi2		   = DB::select('DIGITPAPER_CONTADOR_DUPLICADOS_XTIPO ?', array($id_setarchivo));		
		 
		
				
		return  view('archivosxpropuesta', ['archi'      				=> $archi,
											'contador_duplicados'       => $archi2[0]->contador_duplicados
											]);
		
		
    }
	
	 public function eliminar($id_archivo, $Nombre, $FechaCarga, $Tamano, $Estado, $Tipo, $Propuesta)
    {
        
		//$archi         = DB::select('exec DIGITPAPER_BUSCAR_ARCHIVOS_ASOCIADOS_PROPUESTA ?', array($id_setarchivo));
				
				
		return  view('eliminararchivo',[
										'id_archivo'        				=> $id_archivo,
										'Nombre'        					=> $Nombre,
										'FechaCarga'        				=> $FechaCarga,
										'Tamano'        					=> $Tamano,
										'Estado'        					=> $Estado,
										'Tipo'        						=> $Tipo,
										'Propuesta'        					=> $Propuesta
										]);
		
		
    }
	
	
	
	 public function confirmar_duplicado($setarchivoid)
    {
        		
		$archi         = DB::select('exec DIGITPAPER_DUPLICADOS_XSET ?', array($setarchivoid));	
		
		return  view('eliminar_duplicados',['archi'  => $archi	]);	
		
    }
	
	
	 public function actualizar($Id)
    {
        			
		$archi         = DB::select('exec DIGITPAPER_BUSCAR_ARCHIVOS_ASOCIADOS_PROPUESTA ?', array($Id));	
		
		return  view('corregirarchivo',['archi'  => $archi	]);	
		
    }
	
	 public function actualizar_archivo($compania,$npropuesta,$tipoarchivo,$id_archivo)
    {
             	
		
		/*$compania         = $_POST["compania"];	
		$npropuesta         = $_POST["npropuesta"];	
		$tipoarchivo        = $_POST["tipoarchivo"];
		$id_archivo         = $_POST["id_archivo"];*/
		
		
		
		$archi         = DB::select('exec DIGITPAPER_OBTENER_NUEVO_NOMBRE ?,?,?,?', array($id_archivo,$compania,$npropuesta,$tipoarchivo));
		
		$move   	   = 0;
		
		
		
		
		
		if (($archi[0]->existe_propuesta>0) && (\File::exists($archi[0]->nueva_ruta)) && (\File::exists($archi[0]->ruta_actual)))		 
		{   
			$move          =\File::move($archi[0]->ruta_actual,($archi[0]->nueva_ruta).($archi[0]->nuevo_nombre));
			
			//$delete        =\File::delete($archi[0]->ruta_actual);
		}
		else
		{
		
			if (($archi[0]->existe_propuesta>0) && (!(\File::exists($archi[0]->nueva_ruta)) || !(\File::exists($archi[0]->ruta_actual))))
			{   
				$respuesta="Uno o varios archivos no fueron encontrados en el directorio físico";
			}
		}
		
		if ($archi[0]->existe_propuesta==0)
		{   
			
			$ruta_compania= str_replace("\\".$archi[0]->NroPropuesta,"",$archi[0]->nueva_ruta);
			$ruta_compania= substr($ruta_compania,0,-1);
			if(\File::exists($ruta_compania))
			{   
				$directorio_propuesta= \File::makeDirectory($ruta_compania."\\".$archi[0]->NroPropuesta,0777);
				
				if ($directorio_propuesta==1)
				{  		        				
					$move          =\File::move($archi[0]->ruta_actual,($archi[0]->nueva_ruta).($archi[0]->nuevo_nombre));					
				}
				else
				{    	
					$respuesta="No se pudo crear el directorio para la propuesta";
				}
				
				
			}
			else
			{
				
				echo($ruta_compania);
				$directorio_compania = \File::makeDirectory($ruta_compania,0777);				
				$directorio_propuesta= \File::makeDirectory($ruta_compania."\\".$archi[0]->NroPropuesta,0777);
				echo("entre");
				
				if (($directorio_compania==1) && ($directorio_propuesta==1))
				{   
			        				
					$move          =\File::move($archi[0]->ruta_actual,($archi[0]->nueva_ruta).($archi[0]->nuevo_nombre));
					
				}
				else
				{
					
					$respuesta="No se pudieron crear el/los directorio(s)";
				}
			}
		}
		
		if ($move==1)
		{   
			$update=DB::select('exec DIGITPAPER_CORREGIR_ARCHIVO_ERROR_BD ?,?,?,?,?,?', array($id_archivo,$compania,$archi[0]->NroPropuesta,$tipoarchivo,$archi[0]->nuevo_nombre,$archi[0]->nueva_ruta));
			$respuesta	= "Archivo actualizado con Éxito";
			
			
		}
		else
		{   
			$respuesta	= "No se pudo actualizar la ruta física del archivo";
		}
		
		
		return  view('respuesta',['codres'  	=> $move,
								  'respuesta'	=> $respuesta
								  ]);	
		
    }
	
	
	
	
	 public function eliminar_archivo($id_archivo)
    {
        
		$archi         = DB::select('exec DIGITPAPER_ELIMINAR_ARCHIVOS_DUPLICADOS ?', array($id_archivo));
		
		$archi2		   = DB::select('exec DIGITPAPER_BUSCAR_ELIMINADOS_XSET ?',array($archi[0]->setarchivoid));
		
		
		
		 if($archi2[0]->Nombre=!"")
		 {			
			foreach($archi2 as $archivo) 
			{
			   $move          =\File::move($archivo->Ruta,$archivo->NuevaRuta);
			}
			
			$move			  =\File::move($archi[0]->rutaactual,$archi[0]->rutanueva);	
			
			$respuesta= 'Archivos Duplicados Eliminados Exitosamente';
			
			
				
		 }
		 else
		 { 
           $respuesta= 'Los Archivos no pudieron ser eliminados';
		   $move=0;
			
         }   
		
		
		return  view('respuesta',['codres'  	=> $move,
								  'respuesta'	=> $respuesta
								  ]);	
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		echo("Entre destroy");
    }
}

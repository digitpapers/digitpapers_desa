@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('Digitpapers-Página Principal') }} 
@endsection

@section('namelg1_title')  <!-- Titulo menú lado izquierdo extendido -->
	{{ trans('Digit') }}
@endsection

@section('namelg2_title')
	{{ trans('Papers') }}
@endsection

@section('namemini1_title') <!-- Titulo menú lado izquierdo reducido -->
	{{ trans('D') }}
@endsection

@section('namemini2_title')
	{{ trans('P') }}
@endsection

@section('contentheader_title') <!-- Titulo Panel Derecho esquina superior izquierda -->
  {{ trans('Dashboard') }}
@endsection

@section('contentheader_description')
 <!-- {{ trans('Control Panel') }}-->
  
@endsection


@section('main-content')
	<!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$archivos_cargados_dia}}</h3> 
              <p>Archivos Carga Diaria</p>
            </div>
            <div class="icon">
              <i class="ion ion-upload"></i>
            </div>
            <a href="#" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$tasafallo}}<sup style="font-size: 20px">%</sup></h3>

              <p>Tasa de Fallo</p>
            </div>
            <div class="icon">
              <i class="ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box       <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>    -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$tiempo_proceso}}seg</h3>

              <p>Tiempo de Procesamiento</p>
            </div>
            <div class="icon">
              <i class="ion ion-speedometer"></i>
            </div>
            <a href="" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$totalarchivos}}</h3>

              <p>Archivos Totales</p>
            </div>
            <div class="icon">
              <i class="ion ion-folder"></i>
            </div>
            <a href="#" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
	  
	   <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Reporte de Archivos Cargados por Mes vs Archivos con error</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <!--<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>-->
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <p class="text-center">
                    <strong>Archivos Cargados: {{$primer_mes_grafico}}, {{$anio}}- {{$tercer_mes_grafico}}, {{$anio_actual}}</strong>
                  </p>

                  <div class="chart">
                    <!-- Sales Chart Canvas -->
                    <canvas id="salesChart" style="height: 180px;"></canvas>
					<div id="primer_mes" data={{$primer_mes_grafico}}></div>
					<div id="segundo_mes" data={{$segundo_mes_grafico}}></div>
					<div id="tercer_mes" data={{$tercer_mes_grafico}}></div>
					
					<div id="cantidad_archivos_mes1" data={{$archivos_cargados_mes1}}></div>
					<div id="cantidad_archivos_mes2" data={{$archivos_cargados_mes2}}></div>
					<div id="cantidad_archivos_mes3" data={{$archivos_cargados_mes}}></div>
					
					<div id="cantidad_archivos_mes1_conerror" data={{$archivos_cargados_mes1_conerror}}></div>
					<div id="cantidad_archivos_mes2_conerror" data={{$archivos_cargados_mes2_conerror}}></div>
					<div id="cantidad_archivos_mes3_conerror" data={{$archivos_cargados_mes_conerror}}></div>
					
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <p class="text-center">
                    <strong>Cargas por Compañía</strong>
                  </p>
				  <!-- /.progress-group -->   
				  <div class="progress-group">
                    <span class="progress-text">BCI</span>
                    <span class="progress-number"><b>{{$cont_bci}}</b>/{{$totalarchivos}}</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: {{$porc_bci}}%"></div>
                    </div>
                  </div>
				  
				  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Chilena</span>
                    <span class="progress-number"><b>{{$cont_chilena}}</b>/{{$totalarchivos}}</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: {{$porc_chilena}}%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
				  
				  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Consorcio</span>
                    <span class="progress-number"><b>{{$cont_consorcio}}</b>/{{$totalarchivos}}</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="width: {{$porc_consorcio}}%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->

                  <div class="progress-group">
                    <span class="progress-text">HDI</span>
                    <span class="progress-number"><b>{{$cont_hdi}}</b>/{{$totalarchivos}}</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-cyan" style="width: {{$porc_hdi}}%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Liberty</span>
                    <span class="progress-number"><b>{{$cont_liberty}}</b>/{{$totalarchivos}}</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: {{$porc_liberty}}%"></div>
                    </div>
                  </div>
                  		  
				  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Sura</span>
                    <span class="progress-number"><b>{{$cont_sura}}</b>/{{$totalarchivos}}</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-magenta" style="width: {{$porc_sura}}%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
				   <div class="progress-group">
                    <span class="progress-text">Errores</span>
                    <span class="progress-number"><b>{{$total_errores}}</b>/{{$totalarchivos}}</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: {{$porcentaje_fallo_mes}}%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
				  
				  
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row" >
                <div class="col-sm-4 col-xs-6" >
                  <div class="description-block border-right" >
				    <h5 class="description-header text-info"  >VCarga {{$velocidad_carga_mes1}} seg<i class="fa fa-caret-up"></i></h5>
                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> Fallo {{$porcentaje_fallo_mes1}}%</span>                    
                    <br><span class="description-text">{{$primer_mes_grafico}}</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 col-xs-6" >
                  <div class="description-block border-right">
				    <h5 class="description-header text-info">VCarga {{$velocidad_carga_mes2}} seg<i class="fa fa-caret-up"></i></h5>
                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> Fallo {{$porcentaje_fallo_mes2}}%</span>                    
                    <br><span class="description-text">{{$segundo_mes_grafico}}</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 col-xs-6" >
                  <div class="description-block border-right">
				    <h5 class="description-header text-info">VCarga {{$velocidad_carga_mes}} seg <i class="fa fa-caret-up"></i></h5>
                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> Fallo {{$porcentaje_fallo_mes}}%</span>                    
                    <br><span class="description-text">{{$tercer_mes_grafico}}</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
               
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->   




	   
	  <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Porcentaje de Fallos </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <div class="chart-responsive">
                    <canvas id="pieChart" height="150"></canvas>
					<div id="total_errores" data={{$total_errores}}></div>
					<div id="total_archivos_sinerror" data={{$total_archivos_sinerror}}></div>
					<div id="total_archivos_duplicados" data={{$total_duplicados}}></div>
                  </div>
                  <!-- ./chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <ul class="chart-legend clearfix">
                    <li><i class="fa fa-circle-o text-red"></i> Archivos con error</li>                    
                    <li><i class="fa fa-circle-o text-aqua"></i> Archivos Válidos</li> 
					 <li><i class="fa fa-circle-o text-green"></i> Archivos Duplicados</li> 
                  </ul>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a >% Archivos con error
                  <span class="pull-right text-red"><i class="fa fa-angle-down"></i> {{$tasafallo}}%</span></a>
				</li>
                <li><a >% Archivos Válidos <span class="pull-right text-aqua"><i class="fa fa-angle-up"></i> {{$tasa_aciertos}}%</span></a>
                </li> 
				<li><a >% Archivos Duplicados <span class="pull-right text-green"><i class="fa fa-angle-up"></i> {{$tasa_duplicados}}%</span></a>
                </li>
              </ul>
            </div>
            <!-- /.footer -->
          </div>
          <!-- /.box -->
	  
	  
	  
	  </section>
    <!-- /.content -->
  
@endsection

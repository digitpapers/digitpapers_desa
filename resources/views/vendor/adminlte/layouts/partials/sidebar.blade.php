<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        
		 
		
		
        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/img/derco_logo_fondo_blanco.png" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p style="overflow: hidden;text-overflow: ellipsis;max-width: 160px;" data-toggle="tooltip" title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">{{ trans('Home DigitPapers') }}</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="{{ url('home') }}"><i class='fa fa-link'></i> <span>{{ trans('Dashboard') }}</span></a></li>
		<!--	 <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>{{ trans('Consultas') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">{{ trans('Archivos Procesados') }}</a></li>
                    <li><a href="#">{{ trans('Archivos No Procesados') }}</a></li>
                </ul>
            </li>
			
			<li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>{{ trans('Operaciones con Archivo') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">{{ trans('Descargar') }}</a></li>
                    <li><a href="#">{{ trans('Renombrar') }}</a></li>
					<li><a href="#">{{ trans('Enviar por email') }}</a></li>
                </ul>
            </li>
             <li><a href="#"><i class='fa fa-link'></i> <span>{{ trans('Consulta de Archivo') }}</span></a></li> -->
           
        </ul><!-- /.sidebar-menu -->
		
		<!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <!--- el resto del código -->
            <li><a href="/products"><i class='fa fa-link'></i> <span>{{ trans('Consulta de Propuestas ') }}</span></a></li>
          </ul><!-- /.sidebar-menu   {!! url('products')  !!}  -->
    </section>
    <!-- /.sidebar -->
</aside>

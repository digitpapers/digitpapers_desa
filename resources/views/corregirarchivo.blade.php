@extends('adminlte::layouts.app2')
 
@section('htmlheader_title')
	{{ trans('Digitpapers-Actualizar Archivos') }} 
@endsection

@section('namelg1_title')  <!-- Titulo menú lado izquierdo extendido -->
	{{ trans('Digit') }}
@endsection

@section('namelg2_title')
	{{ trans('Papers') }}
@endsection

@section('namemini1_title') <!-- Titulo menú lado izquierdo reducido -->
	{{ trans('D') }}
@endsection

@section('namemini2_title')
	{{ trans('P') }}
@endsection

@section('contentheader_title') <!-- Titulo Panel Derecho esquina superior izquierda -->
  {{ trans('Corrección de Errores') }}
@endsection

@section('contentheader_description')
<!--  {{ trans('Panel') }} -->
  
@endsection
 
@section('main-content')
 
    <div class="container-fluid spark-screen">      
		<div class="row">
            <div class="col-lg-12 col-xs-5">
                <div class="panel panel-default">
                    <div class="panel-heading">{{trans('Actualizar Datos para Archivo:')}} <b>{{$archi[0]->NombreInicio}}</b>  &nbsp &nbsp &nbsp Fecha de Carga: &nbsp<b>{{$archi[0]->FechaCarga}} </b> &nbsp
					  
					  <a href="/download/{{$archi[0]->Id}}/{{$archi[0]->NombreInicio}}" target="_blank">
								<button type="button" class="btn btn-primary btn-xs " >
								<span class="glyphicon glyphicon-search" ></span> 
							  </button>
							</a>
					
					</div>
 
                    <div class="panel-body">
                        <div class="box-body">
                            <div class="form-group">
                              <form name="upd_file" id="upd_file"> 

							        <div class="row">
									  <div class="col-md-2">
									   <label for="compania">Compañía</label>
									  </div>
									   <div class="col-md-4">
									   <select class="form-control" id="compania" name="compania">
													<option value="">Seleccionar Compañía</option> 
													<option value="BCI">BCI</option> 
													<option value="Chilena">Chilena</option>
													<option value="Consorcio">Consorcio</option>
													<option value="HDI">HDI</option>
													<option value="Liberty">Liberty</option> 													
													<option value="Sura">Sura</option>
													
										</select>
									  </div>
									 </div> 
									 <div class="row"> <p></p>	 </div>
									 
									 <div class="row"> 
									  <div class="col-md-2">
									   <label for="npropuesta">Nro de Propuesta</label>
									  </div>
									  <div class="col-md-4">
									   <input type="text" name="npropuesta" id="npropuesta" placeholder="Introduzca Nro Propuesta" class="form-control name_list" />
									  </div>
									  <div class="col-md-4">
									   <input type="text" name="id_archivo" id="id_archivo" value={{$archi[0]->Id}} class="form-control name_list" style="visibility:hidden" />
									  </div>
									 </div> 							
									 
									 <div class="row"> <p></p>	 </div>

									
									<div class="row">
									  <div class="col-md-2">
									     <label for="estado">Tipo Archivo</label>
									  </div>
									  <div class="col-md-4">
									   <select class="form-control" id="tipoarchivo" name="tipoarchivo">
													<option value="">Seleccione Tipo de Archivo</option> 											 
													<option value="Cedula_de_identidad">Cédula de Identidad</option>
													<option value="Cheque">Cheque</option>
													<option value="Cheque">Factura</option>	
													<option value="Mandato">Mandato</option>
													<option value="Set">Set</option> 
										</select>
									  </div>									 								  
									</div>
									
									<div class="row"> <p></p>   </div>											
									
									<div class="row">								  								
										<!--<div class="col-md-4">
									     <a href="/actualizar_archivo/" >				
										   <button type="button" class="btn btn-primary">Actualizar</button>
										 </a>-->
										 <div class="col-md-4">
										 <a href="{{ URL::previous() }}" >				
										  <button type="button" class="btn btn-outline-info">Regresar</button>
										 </a>
										 
										 <input type="button" name="submit" id="submit" class="btn btn-primary" value="Actualizar" onclick="Confirmar()"  />
										 
										  
										 
									  </div> <!-- Los filtros para este botón se configuran en el archivo scripts2.blade.php en la carpeta views/layouts/partial del acatcha -->
									  
									</div>
                                </form>  
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
	
		  
	</div>
	
@endsection
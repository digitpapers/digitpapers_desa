@extends('adminlte::layouts.app2')
 
@section('htmlheader_title')
	{{ trans('Digitpapers-Archivos x Propuesta') }} 
@endsection

@section('namelg1_title')  <!-- Titulo menú lado izquierdo extendido -->
	{{ trans('Digit') }}
@endsection

@section('namelg2_title')
	{{ trans('Papers') }}
@endsection

@section('namemini1_title') <!-- Titulo menú lado izquierdo reducido -->
	{{ trans('D') }}
@endsection

@section('namemini2_title')
	{{ trans('P') }}
@endsection

@section('contentheader_title') <!-- Titulo Panel Derecho esquina superior izquierda -->
  {{ trans('Eliminar Duplicados') }}
@endsection

@section('contentheader_description')
 <!-- {{ trans('Panel') }} -->
  
@endsection
 
@section('main-content')	
	
	<div class="row">
	 <div class="col-lg-12 col-xs-5">
	     <div class="box">
            <div class="box-header">
              <h3 class="box-title">Archivos Tipo Set Duplicados en Propuesta: &nbsp <b>{{$archi[0]->npropuesta}}</b></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Archivo Válido</th>
                  <th>Fecha Carga</th>                  
				  <th>Tamaño (Kb)</th>
				  <th>Nombre Archivo</th>
                  <th>Estado</th>
                  <th>Tipo Archivo</th>			  
                </tr>
                </thead>
                <tbody>
				   
                   @if(!empty($archi))  
					  @foreach($archi as $archivo)  
				        @if($archivo->estadoarchivoid!=11)
							  <tr>
								<td width="10%"> 						  
								  <input type="checkbox" name="eliminado" value={{$archivo->id}} >							
								</td>
								<td>{{$archivo->fechacarga}}</td>						
								<td>{{$archivo->tamano}}</td>
								<td>{{$archivo->nombrefin}}</td>
								<td width="10%">
								   @if($archivo->estadoarchivoid==1)
									Válido
								   @endif
								   @if ($archivo->estadoarchivoid==9)
										Duplicado
								   @endif
								   @if(($archivo->estadoarchivoid!=1) && ($archivo->estadoarchivoid!=9))
										Error
								   @endif					
								</td>
								<td>
								@if($archivo->tipoarchivoid==1) Set    					@endif
								@if($archivo->tipoarchivoid==2) Mandato   				@endif
								@if($archivo->tipoarchivoid==3) Cédula de Identidad 	@endif
								@if($archivo->tipoarchivoid==4) Cheque				    @endif
								@if($archivo->tipoarchivoid==5) Factura				    @endif
								
								</td>										
								
							   </tr>
							@endif
					   @endforeach 
					   @else
					   <tr>
						<td colspan="8">No hay registro !!</td>
					  </tr>
					  @endif
                
                </tbody>
                <tfoot>
              <!--  <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
				  <th>X</th>
                </tr>-->
                </tfoot>
              </table>
			   <div class="row">
					<p></p>			    
			  </div>
			  <div class="row">									 
				<div class="col-md-1">
				     <a href="{{ URL::previous() }}" >				
						   <button type="button" class="btn btn-outline-info">Regresar</button>
					 </a>
				</div>
									  
			    <div class="col-md-1">
					  <input type="button" name="submit" id="submit" class="btn btn-primary" value="Aplicar" onclick="eliminar_duplicados()"  />
					  
				</div>			   	
				
		      </div>
			  <div class="row">
					<p></p>			    
			  </div>
			  
            </div>
            <!-- /.box-body -->		
			
			</div>
          </div>
	</div>
	
	
	
	
@endsection
@extends('adminlte::layouts.app2')
 
@section('htmlheader_title')
	{{ trans('Digitpapers-Archivos x Propuesta') }} 
@endsection

@section('namelg1_title')  <!-- Titulo menú lado izquierdo extendido -->
	{{ trans('Digit') }}
@endsection

@section('namelg2_title')
	{{ trans('Papers') }}
@endsection

@section('namemini1_title') <!-- Titulo menú lado izquierdo reducido -->
	{{ trans('D') }}
@endsection

@section('namemini2_title')
	{{ trans('P') }}
@endsection

@section('contentheader_title') <!-- Titulo Panel Derecho esquina superior izquierda -->
  {{ trans('Consultas de Archivos') }}
@endsection

@section('contentheader_description')
 <!-- {{ trans('Panel') }} -->
  
@endsection
 
@section('main-content')	
	
	<div class="row">
	 <div class="col-lg-12 col-xs-5">
	     <div class="box">
            <div class="box-header">
              <h3 class="box-title">Archivos Contenidos en Propuesta: &nbsp <b>{{$archi[0]->NroPropuesta}}</b></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Opciones</th>
                  <th>Fecha Carga</th>                  
				  <th>Tamaño (Kb)</th>
				  <th>Nombre Archivo</th>
                  <th>Estado</th>
                  <th>Tipo Archivo</th>					 
                </tr>
                </thead>
                <tbody>
				   
                   @if(!empty($archi))  
					  @foreach($archi as $archivo)  
				          @if ($archivo->EstadoArchivoId!=11)
							  <tr>
								<td width="10%"> 						  
								  
									 
									 <a href="/download/{{$archivo->Id}}/{{$archivo->NombreFin}}" target="_blank">
										<button type="button" class="btn btn-primary btn-xs " >
										<span class="glyphicon glyphicon-search" ></span> 
									  </button>
									</a>
									<!--
									<a href="/download/{{$archivo->Id}}" >
										<button type="button" class="btn btn-warning btn-xs" >
										<span class="glyphicon glyphicon-pencil"></span> 
									  </button>
									</a>
									<a href="/eliminar/{{$archivo->Id}}/{{$archivo->NombreFin}}/{{$archivo->FechaCarga}}/{{$archivo->Tamano}}/{{$archivo->EstadoArchivoId}}/{{$archivo->TipoArchivoId}}/{{$archi[0]->NroPropuesta}}" >
										<button type="button" class="btn btn-danger btn-xs" >
										<span class="glyphicon glyphicon-trash"></span> 
									  </button>
									</a> -->
											 
								
									
								
								</td>
								<td>{{$archivo->FechaCarga}}</td>						
								<td>{{$archivo->Tamano}}</td>
								<td>{{$archivo->NombreFin}}</td>
								<td width="10%">
								   @if($archivo->EstadoArchivoId==1)
									Ok
								   @endif
								   @if ($archivo->EstadoArchivoId==9)
										Duplicado
								   @endif
								   @if ($archivo->EstadoArchivoId==10)
										Corregido
								   @endif
								   @if(($archivo->EstadoArchivoId!=1) && ($archivo->EstadoArchivoId!=9) && ($archivo->EstadoArchivoId!=10))
										Error
								   @endif					
								</td>
								<td>
								@if($archivo->TipoArchivoId==1) Set    					@endif
								@if($archivo->TipoArchivoId==2) Mandato   				@endif
								@if($archivo->TipoArchivoId==3) Cédula de Identidad 	@endif
								@if($archivo->TipoArchivoId==4) Cheque				    @endif
								@if($archivo->TipoArchivoId==5) Factura				    @endif
								
								</td>	
								
								
							   </tr>
						  @endif
					   @endforeach 
					   @else
					   <tr>
						<td colspan="8">No hay registro !!</td>
					  </tr>
					  @endif
                
                </tbody>
                <tfoot>
              <!--  <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
				  <th>X</th>
                </tr>-->
                </tfoot>
              </table>
			   <div class="row">
					<p></p>			    
			  </div>
			  <div class="row">									 
				<div class="col-md-1">
				     <a href="{!! url('products')  !!}" >				
						   <button type="button" class="btn btn-outline-info">Regresar</button>
					 </a>
				</div>
									  
			    <div class="col-md-1">
					  @if (($contador_duplicados)>1)
					     <input type="button" name="submit" id="submit" class="btn btn-primary" value="Corregir Duplicados" onclick="Confirmar_duplicados({{$archi[0]->SetArchivoId}})"  />
					  @else
						 <input type="button" name="submit" id="submit" class="btn btn-primary" value="Corregir Duplicados" onclick="" disabled /> 
					  @endif
				</div>			   	
				
		      </div>
			  <div class="row">
					<p></p>			    
			  </div>
			  
            </div>
            <!-- /.box-body -->		
			
			</div>
          </div>
	</div>
	
	
	
	
@endsection
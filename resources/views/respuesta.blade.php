@extends('adminlte::layouts.app2')
 
@section('htmlheader_title')
	{{ trans('Digitpapers-Actualizar Archivos') }} 
@endsection

@section('namelg1_title')  <!-- Titulo menú lado izquierdo extendido -->
	{{ trans('Digit') }}
@endsection

@section('namelg2_title')
	{{ trans('Papers') }}
@endsection

@section('namemini1_title') <!-- Titulo menú lado izquierdo reducido -->
	{{ trans('D') }}
@endsection

@section('namemini2_title')
	{{ trans('P') }}
@endsection

@section('contentheader_title') <!-- Titulo Panel Derecho esquina superior izquierda -->
  {{ trans('Resultado ') }}
@endsection

@section('contentheader_description')
 <!-- {{ trans('Panel') }} -->
  
@endsection
 
@section('main-content')

    <div class="container-fluid spark-screen">      
		<div class="row">
            <div class="col-lg-12 col-xs-5">
                <div class="panel panel-default">
                    <div class="panel-heading">Mensaje 
					</div>
 
                    <div class="panel-body">
                        <div class="box-body">
                            <div class="form-group">
                              <form name="upd_file" id="upd_file"> 

							        <div class="row">
										<div class="col-md-6">
											 @if($codres==1)
											   <div class="pull-left image">
												    
													<!--<img src="/img/OK.png" class="img-circle" alt="User Image" />-->
												 	<a>
														<button type="button" class="btn btn-success btn-xs ">
														<span class="glyphicon glyphicon-ok"></span> 
														</button>
													</a>											 
												 <label for="npropuesta">{{$respuesta}}</label>
											   </div>	
											 @endif
											 @if($codres==0)
											   <div class="pull-left image">
												 <!--<img src="/img/fracaso.png" class="img-circle" alt="User Image" />-->
													<a>
														<button type="button" class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-remove"></span> 
														</button>
													</a>
												 <label for="npropuesta">{{$respuesta}}</label>
											   </div>	
											 @endif
										 </div> 
										 <div class="col-md-6">
											
										</div>
									 </div>
									 
									
									 
									 
									 
									 <div class="row"> <p></p>	 </div>
									 
									 							
									 
									 <div class="row"> <p></p>	 </div>								
									
									
									<div class="row"> <p></p>   </div>											
									
									<div class="row">										
																				 
										  <a href="{!! url('products')  !!}" >				
										  <button type="button" class="btn btn-outline-info">Regresar</button>
										 
									  </div> 
									  
									</div>
                                </form>  
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
	
		  
	</div>
	
@endsection
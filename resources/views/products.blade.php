@extends('adminlte::layouts.app2')
 
@section('htmlheader_title')
	{{ trans('Digitpapers-Consultas') }} 
@endsection

@section('namelg1_title')  <!-- Titulo menú lado izquierdo extendido -->
	{{ trans('Digit') }}
@endsection

@section('namelg2_title')
	{{ trans('Papers') }}
@endsection

@section('namemini1_title') <!-- Titulo menú lado izquierdo reducido -->
	{{ trans('D') }}
@endsection

@section('namemini2_title')
	{{ trans('P') }}
@endsection

@section('contentheader_title') <!-- Titulo Panel Derecho esquina superior izquierda -->
  {{ trans('Consultas de Digitalizaciones') }}
@endsection

@section('contentheader_description')
 <!-- {{ trans('Panel') }} -->
  
@endsection
 
@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-lg-12 col-xs-5">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('Filtrar') }}</div>
 
                    <div class="panel-body">
                        <div class="box-body">
                            <div class="form-group">
                              <form name="add_name" id="add_name"> 

							        <div class="row">
									  <div class="col-md-4">
									   <label for="compania">Compañía</label>
									  </div>
									  <div class="col-md-4">
									   <label for="npropuesta">Nro de Propuesta</label>
									  </div>
									  <div class="col-md-4">
									     <label for="estado">RUT</label>
									  </div>
									</div>
									
									<div class="row">
									  <div class="col-md-4">
									   <select class="form-control" id="compania" name="compania">
													<option value="">Seleccionar Compañía</option> 
													<option value="BCI">BCI</option> 													
													<option value="Chilena">Chilena</option>
													<option value="Consorcio">Consorcio</option>
													<option value="HDI">HDI</option>
													<option value="Liberty">Liberty</option> 
													<option value="Sura">Sura</option>
													
										</select>
									  </div>
									  <div class="col-md-4">
									   <input type="text" name="npropuesta" id="npropuesta" placeholder="Introduzca Nro Propuesta" class="form-control name_list" />
									  </div>
									   <div class="col-md-4">
									   <input type="text" name="nrut" id="nrut" placeholder="Introd. RUT sin dv y guiones Ej: 12345678" class="form-control name_list" />
									  </div>
									</div>
									
									<div class="row">
									  
									  <p></p>   
									  
									</div>
									
									 <div class="row">
									  <div class="col-md-4">
									     <label for="estado">Estado</label>
									  </div>									 
									  <div class="col-md-4">
									   <label for="fini">Fecha Inicio</label>
									  </div>
									  <div class="col-md-4">
									   <label for="ffin">Fecha Fin</label>
									  </div>
									  
									</div>
									
									<div class="row">
									 <div class="col-md-4">
									     <select class="form-control" id="estado" name="estado">
													<option value="">Seleccionar Estado</option> 
													<option value="Corregido">Corregido</option>
													<option value="Corregido y Valido">Corregidos y Válidos</option>
													<option value="Duplicado">Duplicado</option> 
													<option value="Error">Error</option> 
													<option value="Valido">Válido</option> 
																	
										</select>
									  </div>									
									  <div class="col-md-4">
									   <input type="date" name="fini" id="fini" step="1" min="2018-01-01" max="2030-12-31" class="form-control name_list" value="">
									  </div>
									  <div class="col-md-4">
									   <input type="date" name="ffin" id="ffin" step="1" min="2018-01-01" max="2030-12-31" class="form-control name_list" value="">
									  </div>
									  
									</div>
									
									<div class="row">
									  
									  <p></p>   
									  
									</div>
									
									<div class="row">
									  <div class="col-md-4">
									     
									  </div>
									  <div class="col-md-4">
									    
									  </div>									
										<div class="col-md-4">
									     <input type="button" name="submit" id="submit" class="btn btn-primary" value="Buscar" onclick="Buscar_Propuestas()"  />
										 
										 										 
									  </div> <!-- Los filtros para este botón se configuran en el archivo scripts2.blade.php en la carpeta views/layouts/partial del acatcha -->
									  
									</div>
                                </form>  
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="row">
	 <div class="col-lg-12 col-xs-5">
	     <div class="box">
            <div class="box-header">
              <h3 class="box-title">Resultados</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Opciones</th>
                  <th>Compañía</th>
                  <th>Propuesta</th>
				  <th>RUT</th>
				  <th>Nombre Archivo</th>
                  <th>Fecha de Carga</th>
                  <th>Cantidad Archivos</th>
				  <th>Estado</th>
				  <th>Mensaje</th>
				  
				  
                </tr>
                </thead>
                <tbody>
				   
                   @if (!empty($archivos) )
					  @foreach($archivos as $archivo)  
				        
							  <tr>
								<td width="8%"> 
								  <div class="btn-group" role="group" aria-label="Basic example">
								  
									 @if(($archivo->CodError==0) || ($archivo->CodError==8) || ($archivo->CodError==9))
										 <a href="/vercontenido/{{$archivo->Id}}" >
											<button type="button" class="btn btn-primary btn-xs">
											<span class="glyphicon glyphicon-folder-open"></span> 
										  </button>
										</a>
									@endif
									@if(($archivo->CodError!=0) && ($archivo->CodError!=8) && ($archivo->CodError!=9))
										<a href="/actualizar/{{$archivo->Id}}/{{$archivo->FechaCarga}}/{{$archivo->CodError}}" >
											<button type="button" class="btn btn-warning btn-xs" >
											<span class="glyphicon glyphicon-wrench"></span> 
										  </button>
										</a>
									@endif
								 </div>
								 <!--<div class="col-md-2">
									<button type="button" class="btn btn-danger btn-xs">
										<span class="glyphicon glyphicon-remove"></span> Borrar
									  </button>-->
									<!--<input type="button" name="submit" id="submit" class="btn btn-success btn-xs" value="Mail" />-->
									
								 </div>
								</td>
								<td>
								  @if ($archivo->CodCompania==16)
									  BCI
								  @endif
								  @if ($archivo->CodCompania==24)
									  Liberty
								  @endif
								  @if ($archivo->CodCompania==03)
									  Chilena
								  @endif
								  @if ($archivo->CodCompania==17)
									  Consorcio
								  @endif
								  @if ($archivo->CodCompania==12)
									  Sura
								  @endif
								  @if ($archivo->CodCompania==04)
									  HDI
								  @endif
								 
								  
								
								</td>
								<td>{{$archivo->NroPropuesta}}</td>
								<td>{{$archivo->RutCliente}}</td>
								<td>{{$archivo->NroPropuesta}}</td>
								<td width="10%">{{substr($archivo->FechaCarga,8,2)."-".substr($archivo->FechaCarga,5,2)."-".substr($archivo->FechaCarga,0,4)}}</td>
								<td>{{$archivo->NroArchivosTotal}}</td>
								<td>
								@if($archivo->CodError==0)
									Válido
								@endif
								@if ($archivo->CodError==8)
									Duplicado
								@endif
								@if ($archivo->CodError==9)
									Corregido
								@endif
								@if(($archivo->CodError!=0) && ($archivo->CodError!=8) && ($archivo->CodError!=9))
									Error
								@endif
								
								</td>
								<td>{{$archivo->MsgError}} </td>
								
													
								
							   </tr>
						
					   @endforeach 
					   @else
					   <tr>
						<td colspan="8">No hay registro !!</td>
					  </tr>
					  @endif
                
                </tbody>
                <tfoot>
              <!--  <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
				  <th>X</th>
                </tr>-->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
			</div>
          </div>
	</div>
@endsection